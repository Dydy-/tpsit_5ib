# Cronometro

In questo progetto i metodi privati, rappresentati dall'underscore, essenziali per la programmazione del cronometro sono "_cronos()", che si occupa di incrementare il tempo tramite delle condizioni. Il metodo "_play()" è necessario per far partire il cronometro quando si clicca sul bottone creato; in aggiunta vi è presente un'animazione. "_azzeratore()", invece, azzera il tempo quando viene premuto il pulsante a destra a forma di orologio, solo quando il cronometro, però, è in pausa.

Nella riga 19 ho inserito la Dark Theme, l'esempio alla quale mi sono riferita è questo:

<pre><code>MaterialApp(
      title: 'Flutter Theming Tutorials',
      debugShowCheckedModeBanner: false,
      theme: ThemeData.light(),
      darkTheme: ThemeData.dark(),
      home: ThemeDemo(),
);</pre></code>

Per l'avanzamento del tempo ho usato uno Stream.periodic, e per gestirlo un oggetto StreamSubscription. Ecco un esempio:

<pre><code>class Ticker {
  Stream<int> tick({int ticks}) {
    return Stream.periodic(Duration(seconds: 1), (x) => ticks - x - 1)
        .take(ticks);
  }
}</pre></code>

Inoltre il TickerProviderStateMixin è necessario per l'implementazione dell'Animation Controller.

<pre><code>Widget build(BuildContext context) => Center(
        child: AnimatedBuilder(
            animation: animation,
            builder: (context, child) => Container(
                  height: animation.value,
                  width: animation.value,
                  child: child,
                ),
            child: child),
      );
}</pre></code>


La durata dell'animazione l'ho impostata di 300 millisecondi.

Nella Stringa di Testo ho messo direttamente la condizione che mette lo zero davanti al numero finché è minore di 10, usando l'if inline.
Inoltre ho cambiato lo stile del font andando a modificare il file pubspec.yaml nella sezione font:

<pre><code>fonts:
    - family: TurretRoad
      fonts:
        - asset: fonts/TurretRoad-Regular.ttf
          weight: 400
        - asset: fonts/TurretRoad-Bold.ttf
          weight: 700
        - asset: fonts/TurretRoad-Light.ttf
          weight: 300
          style: italic

</pre></code>

Per i pulsanti ho utilizzato i FloatingActionButton:

<pre><code>import 'package:flutter/material.dart';
void main() => runApp(MyApp());
class MyApp extends StatelessWidget {
  static const String _title = 'Flutter Code Sample';
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: _title,
      home: MyStatelessWidget(),
    );
  }
}
class MyStatelessWidget extends StatelessWidget {
  MyStatelessWidget({Key key}) : super(key: key);
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text('Floating Action Button'),
      ),
      body: Center(child: const Text('Press the button below!')),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
        },
        child: Icon(Icons.navigation),
        backgroundColor: Colors.green,
      ),
    );
  }
}</pre></code>

Per abbellire la grafica ho aggiunto anche lo splashColor (senza però l'ulitizzo della classe InkWell):

<pre><code>InkWell(
    splashColor: Colors.yellow,
    highlightColor: Colors.blue,
    child: Icon(Icons.add_circle, size: 50),
    onTap: () {
      setState(() {
        _count += 1;
      });
    },
  ),
</pre></code>
