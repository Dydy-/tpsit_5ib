import 'dart:async';
import 'dart:ui';

import 'package:flutter/material.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Cronometro',
      theme: ThemeData(
        primarySwatch: Colors.pink,
        visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      darkTheme: ThemeData(brightness: Brightness.dark),
      home: MyHomePage(title: 'Cronometro'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage>
    with SingleTickerProviderStateMixin {
  Stream<int> ss = new Stream.periodic(Duration(seconds: 1));
  AnimationController _ctrl;

  @override
  void initState() {
    super.initState();
    _ctrl =
        AnimationController(vsync: this, duration: Duration(milliseconds: 300));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            Text(
              "${ore < 10 ? "0$ore" : "$ore"}:${minuti < 10 ? "0$minuti" : "$minuti"}:${secondi < 10 ? "0$secondi" : "$secondi"}",
              style: TextStyle(
                fontFamily: 'TurretRoad',
                fontSize: 40,
                fontWeight: FontWeight.w400,
              ),
            ),
            Row(mainAxisAlignment: MainAxisAlignment.center, children: [
              Container(
                margin: EdgeInsets.all(25),
                alignment: Alignment.bottomRight,
                child: FloatingActionButton(
                  onPressed: _play,
                  backgroundColor: Colors.pink[200],
                  splashColor: Colors.redAccent,
                  child: AnimatedIcon(
                    icon: AnimatedIcons.play_pause,
                    progress: _ctrl,
                    color: Colors.black54,
                  ),
                ),
              ),
              Container(
                  margin: EdgeInsets.all(25),
                child: FloatingActionButton(
                  onPressed: _azzeratore,
                  child: Icon(Icons.timer, color: Colors.black54),
                  backgroundColor: Colors.pink[200],
                  splashColor: Colors.redAccent,
                )
              )
            ])
          ],
        ),
      ),
    );
  }

  StreamSubscription str;
  int ore = 0, minuti = 0, secondi = 0;

  void _cronos() {
    _ctrl.forward();
    str = ss.listen((event) {
      setState(() {
        if (secondi < 59) {
          secondi++;
        } else if (minuti < 59) {
          minuti++;
          secondi = 0;
        } else {
          ore++;
          minuti = 0;
          secondi = 0;
        }
      });
    });
  }

  void _play() {
    if (str == null) {
      _cronos();
    } else if (str.isPaused) {
      str.resume();
      _ctrl.forward();
    } else {
      str.pause();
      _ctrl.reverse();
    }
  }

  void _azzeratore() {
    if (str.isPaused) {
      setState(() {
        ore = 0;
        minuti = 0;
        secondi = 0;
      });
    }
  }
}
